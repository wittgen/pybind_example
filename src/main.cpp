#include <pybind11/pybind11.h>

#include <string>
namespace py = pybind11;

struct Test {
    Test(const std::string &name) : name(name) { }
    std::string name;
};

PYBIND11_MODULE(cmake_example, m) {
py::class_<Test>(m, "Test")
    .def(py::init<const std::string &>())
    .def_readwrite("name", &Test::name);
}
